extends Control


onready var monster_sprite: Sprite = $MonsterSprite
onready var monster_health_bar: ProgressBar = $MonsterHealth
onready var player_health_bar: ProgressBar = $PlayerHealth
onready var attack_button: Button = $AttackButton
onready var label: Label = $Label

var player_total_hp: int
var player_hp: int
var player_attack: int

var monster_total_hp: int
var monster_hp: int
var monster_attack: int

func _ready():
    player_total_hp = 100
    player_hp = player_total_hp
    player_attack = 20

    monster_total_hp = 80
    monster_hp = monster_total_hp
    monster_attack = 40

    update_monster_health_bar()
    update_player_health_bar()


func on_attack_button_pressed():
    # first we attack the monster
    monster_hp -= player_attack
    update_monster_health_bar()
    if monster_hp <= 0:
        label.text = "You defeated the monster"
        attack_button.disabled = true
        return

    # if the monster is still alive, it attacks us
    player_hp -= monster_attack
    update_player_health_bar()
    if player_hp <= 0:
        label.text = "You were defeated"
        attack_button.disabled = true


func update_monster_health_bar():
    var life_percentage = monster_hp * 100.0 / monster_total_hp
    monster_health_bar.value = life_percentage

func update_player_health_bar():
    var life_percentage = player_hp * 100.0 / player_total_hp
    player_health_bar.value = life_percentage
