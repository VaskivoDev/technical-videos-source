extends Control


onready var monster_sprite: Sprite = $MonsterSprite
onready var monster_health_bar: ProgressBar = $MonsterHealth
onready var player_health_bar: ProgressBar = $PlayerHealth
onready var attack_button: Button = $AttackButton
onready var label: Label = $Label

var player_total_hp: int
var player_hp: int
var player_attack: int

var monster_total_hp: int
var monster_hp: int
var monster_attack: int

func _ready():

    var file = File.new()
    file.open("res://data.json", File.READ)
    var json_data = file.get_as_text()
    file.close()

    # label.text = json_data
    # label.text += "\njson_data is String: %s" % json_data is String

    var parse_result: JSONParseResult = JSON.parse(json_data)
    if parse_result.error != OK:
        push_error("Error parsing JSON file")

    var game_data = parse_result.result
    # label.text = str(game_data)
    # label.text += "\ngame_data is a Dctionary %s" % game_data is Dictionary

    player_total_hp = game_data.player_hp
    player_hp = player_total_hp
    player_attack = game_data.player_attack

    var monster_data = game_data.monsters[1]

    monster_total_hp = monster_data.hp
    monster_hp = monster_total_hp
    monster_attack = monster_data.attack

    monster_sprite.texture = load(monster_data.texture)

    update_monster_health_bar()
    update_player_health_bar()


func on_attack_button_pressed():
    # first we attack the monster
    monster_hp -= player_attack
    update_monster_health_bar()
    if monster_hp <= 0:
        label.text = "You defeated the monster"
        attack_button.disabled = true
        return

    # if the monster is still alive, it attacks us
    player_hp -= monster_attack
    update_player_health_bar()
    if player_hp <= 0:
        label.text = "You were defeated"
        attack_button.disabled = true


func update_monster_health_bar():
    var life_percentage = monster_hp * 100.0 / monster_total_hp
    monster_health_bar.value = life_percentage

func update_player_health_bar():
    var life_percentage = player_hp * 100.0 / player_total_hp
    player_health_bar.value = life_percentage
