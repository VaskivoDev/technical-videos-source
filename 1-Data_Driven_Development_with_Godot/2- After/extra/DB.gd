extends Node

var player_data
var monsters

func _ready():

    var file = File.new()
    file.open("res://data.json", File.READ)
    var json_data = file.get_as_text()
    print(json_data)

    var parse_result: JSONParseResult = JSON.parse(json_data)
    if  parse_result.error != OK:
        push_error("Error parsing JSON file")

    var data = parse_result.result
    player_data = {
        "hp": data.player_hp,
        "attack": data.player_attack
    }
    monsters = data.monsters
